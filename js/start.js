$(document).ready(function () {


    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.btn-more-than').addClass('show');
            $('.sticky-header').addClass('show');
        } else {
            $('.btn-more-than').removeClass('show');
            $('.sticky-header').removeClass('show');
        }
    });
    if ($(".btn-more-than").length > 0) {
        $('.btn-more-than').on('click', function () {
            $("html, body").animate({ scrollTop: 0 }, 1000, 'easeInOutExpo');
            return false;
        });
    }

});